import React, { Component } from 'react';
import './App.css';

class Clock extends Component {




  render() {

    var clock = document.createElement("div");
    clock.className = "clock";
    var wskazowkagodzinowa = document.createElement("div");
    wskazowkagodzinowa.className = "hand hours animate";
    var wskazowkaminutowa = document.createElement("div");
    wskazowkaminutowa.className = "hand minutes animate";
    var wskazowkasekundowa = document.createElement("div");
    wskazowkasekundowa.className = "hand seconds animate";

    var textbox = document.createElement("div");
    textbox.className = "textbox";


    clock.appendChild(wskazowkagodzinowa);
    clock.appendChild(wskazowkaminutowa);
    clock.appendChild(wskazowkasekundowa);

    document.body.appendChild(clock);
    document.body.appendChild(textbox);

    function setTime() {
    
      var dzisiaj = new Date();
      
      var seconds = dzisiaj.getSeconds(),
          minutes = (dzisiaj.getMinutes() + dzisiaj.getSeconds()/60) * 60,
          hours = (dzisiaj.getHours() + (dzisiaj.getMinutes() + dzisiaj.getSeconds()/60)/60) * 3600;
  
      wskazowkasekundowa.style.animationDuration = "60s";
      wskazowkaminutowa.style.animationDuration = "3600s";
      wskazowkagodzinowa.style.animationDuration = "43200s";
  
      wskazowkasekundowa.style.animationDelay = "-" + seconds + "s";
      wskazowkaminutowa.style.animationDelay = "-" + minutes + "s";
      wskazowkagodzinowa.style.animationDelay = "-" + hours + "s";
    }

    
    function print()
    {   
	    var dzis = new Date(),
		      secs = dzis.getSeconds(),
		      min = dzis.getMinutes(),
		      h = dzis.getHours();

	    if(h < 10)
	    {
		    h = "0" + dzis.getHours();
	    }

	    if(min < 10)
	    {
		    min = "0" + dzis.getMinutes();
	    }

	    if(secs < 10)
	    {
		    secs = "0" + dzis.getSeconds();
      }
      
      document.querySelector(".textbox").innerHTML =  h + ":" + min + ":" + secs;
      
	    setTimeout(print,1000);
    }

    
    return (
      <div>
        <div onLoad = {setTime()}>{print()}</div>
      </div>
    )
  }
}

class App extends Component {
  render() {
    return (
      <Clock/>
    );
  }
}

export default App;